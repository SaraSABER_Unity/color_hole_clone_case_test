﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/// <summary>
/// Color Hole Gamee' s Game Manager
/// </summary>
public class GameManager : MonoBehaviour
{
    #region PRIVATE_ATTRIBUTS
    [SerializeField] float score;
    
    
    [SerializeField]  bool isGameFinished;
    [SerializeField] bool isGameStarted;
    [SerializeField] bool isGameContinued;
    #endregion
    #region PUBLIC_ATTRIBUTS
    public GameObject gameOverGO;
    public GameObject UIGGO;
    public GameObject youWonGO;
    public GameObject levelsGO;
    public GameObject endPositionsPrefabsGO;
    public Text gameScorerttsTextUI;
    public GameObject startPositionsPrefabGO;
    public float resetDelay = 1f;
    public GameObject levelManagerGO;
    public GameObject playerPrefabGO;
    public GameObject soundManagerGO;
    public static GameManager instance = null;
    
    #endregion


    #region PRIVATE_UNITY_METHODS
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }


        Setup();
    }
    // Start is called before the first frame update
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {

    }
    #endregion
    #region PUBLIC_UNITY_METHODS
    #endregion


    #region PRIVATE_NON_MONOBEHAVIORS_METHODS
    //Setup Function
    public void Setup()
    {

        //UIGGO.SetActive(false);
        
        isGameStarted = true;
        isGameContinued = false;
        UIGGO.GetComponentInChildren<GraphicRaycaster>().enabled = false;
        //clonePlayerPrefabGO= Instantiate(playerPrefabGO, startPositionsPrefabGO.transform.position, Quaternion.identity) as GameObject;
        //playerPrefabGO.SetActive(false);
        //A little bug in it and im too lazy to see what it is , i m keeping position and instance and moving the player to start position insteads of instantiate,
        ///playerPrefabGO.transform.SetPositionAndRotation(startPositionsPrefabGO.transform.position, Quaternion.identity);

        //playerPrefabGO.SetActive(true);


    }
    //Game start Function
    
    //Function updateUIS
    public void updateUI()
    {
        ///gameScorerttsTextUI.text = "Points: " + score.ToString();
    }
    //CheckGameOver Functions
    public void CheckGameOver()
    {
        /*if ()
        {
            //////////////////// youWonGO.SetActive(true);

            Time.timeScale = 0.25f;
            playerPrefabGO.SetActive(false);
            isGameStarted = false;
            ////isGameWon = true;
            Invoke("sceneLoad", resetDelay);


        }
        if (hearts < 1)
        {
            /// gameOverGO.SetActive(true);
           //// Instantiate(deathParticlesGO, playerPrefabGO.transform.position, Quaternion.identity);
            Time.timeScale = 0.25f;
            playerPrefabGO.SetActive(false);
            /////Invoke("AnimateDeath", resetDelay);

            Invoke("Reset", resetDelay);


        }
        */

    }




    #endregion
    #region PUBLIC_NON_MONOBEHAVIORS_METHODS
    //Function to load scenes
    public void sceneLoad()
    {
        float actualSceneBuild = SceneManager.GetActiveScene().buildIndex;
        if (actualSceneBuild >= 0 && actualSceneBuild < SceneManager.sceneCountInBuildSettings - 1)//We dont call a scene nt her in cuilui
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

        //Or if we look for another Design techniques for the Game rules and UX experience we could at every end of maze invoke the Map or levels lit

    }
    public void EndedGameLoose()
    {
        
        updateUI();
        UIGGO.GetComponentInChildren<GraphicRaycaster>().enabled = true;
        gameOverGO.SetActive(true);
        isGameContinued = false;
        isGameFinished = true;

    }
    //Function End Game Win
    public void EndedGameWin()
    {
        isGameContinued = false;
        updateUI();
        UIGGO.GetComponentInChildren<GraphicRaycaster>().enabled=true;
        //game1EndWinUI.SetActive(true);
        isGameFinished = true;

    }
    //Function Start Game
    public void StartedGame()
    {
        UIGGO.GetComponentInChildren<GraphicRaycaster>().enabled = false;
        isGameStarted = true;
        isGameFinished = false;
        isGameContinued = false;
        updateUI();
        //hearts = maxhearts;
        //game1HearttsTextUI.text = hearts.ToString();
        score = 0;
    }

    public void ReStartedGame()
    {
        //Project fies wasted (due to an erroor)
        //We need to desactivate collected GO and add them to a list 
        //Then this list is having the GO deactivated when colliding with the deadzone (fall)
        //If we need to start?wich is the same as restarts, then we need to call the gamme function that will set active all those collectables in their positions base
        //we couldwill call a function on startgame/setup() to register position of collectibles 
        //we will also call this function resetPOsotopm() wich ahas a loop then call the reactivate() wich set active function loop trough all the object of the ll
        //In optimized way we can have only one function that calls the game start. gameStarted() only without having startiedGame() and ReStartedGame(((      )
        //////
    }
    //Function Start Game
    public void ContinuedGame()
    {
        UIGGO.GetComponentInChildren<GraphicRaycaster>().enabled = false;
        isGameStarted = true;
        isGameFinished = false;
        isGameContinued = true;
        updateUI();
        //hearts = maxhearts;
        //game1HearttsTextUI.text = hearts.ToString();
        
    }
    //Function Edit Heart
    public void GameLScore(int scoreToAdd)
    {
        updateUI();
        score += scoreToAdd;
        gameScorerttsTextUI.text = score.ToString();
    }
    
    #endregion

    #region TESTS METHODS_OR_SCRIPTS
    #endregion
}
