﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Generals Camera Follower Script :) for smooth movement, attachc camssssss    
/// </summary>
public class CameraFollowScript : MonoBehaviour
{
    #region PRIVATE_ATTRIBUTS
    #endregion
    #region PUBLIC_ATTRIBUTS
    public Transform target;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;
    #endregion


    #region PRIVATE_UNITY_METHODS
    // Start is called before the first frame update
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {

    }
    void FixedUpdate()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;

        //transform.LookAt(target);
    }
    #endregion
    #region PUBLIC_UNITY_METHODS
    #endregion


    #region PRIVATE_NON_MONOBEHAVIORS_METHODS
    #endregion
    #region PUBLIC_NON_MONOBEHAVIORS_METHODS
    #endregion

    #region TESTS METHODS_OR_SCRIPTS
    #endregion



}
