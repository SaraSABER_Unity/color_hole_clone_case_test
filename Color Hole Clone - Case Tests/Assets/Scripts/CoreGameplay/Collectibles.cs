﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Collectibles Scrips
/// </summary>
public class Collectibles : MonoBehaviour
{
    #region PRIVATE_ATTRIBUTS
    #endregion
    #region PUBLIC_ATTRIBUTS
    public int points = 0;
    #endregion


    #region PRIVATE_UNITY_METHODS
    //On Trigger Enter Method for Collider zone Death 
    void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
    }
    #endregion
    #region PUBLIC_UNITY_METHODS
    #endregion


    #region PRIVATE_NON_MONOBEHAVIORS_METHODS

    #endregion
    #region PUBLIC_NON_MONOBEHAVIORS_METHODS
    //Progress Poins Methods
    public void CalculateProgress()
    {
        points++;
        if (points % 10 == 0)
        {
            //We can grow our hole here if we want after each 10 collected objects
            //Script parts for scoreUpdas    

        }
    }
    #endregion

    #region TESTS METHODS_OR_SCRIPTS
    #endregion
}
