﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Death Zonw actions Scripts
/// </summary>
public class DeathZone : MonoBehaviour
{
    #region PRIVATE_ATTRIBUTS
    [SerializeField] GameManager gameManagerInstance;
    #endregion
    #region PUBLIC_ATTRIBUTS
    #endregion


    #region PRIVATE_UNITY_METHODS
    //On Trigger Enter Method for Collider zone Death 
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("NonColorsCollectibles"))
        {
            gameManagerInstance.GameLScore(30);//HJardCoded values due to fast exel
            //vibrate mobiles phone: //Only activate with specifi if (mobile run) otherwise it pro
            //Handheld.Vibrate();

            Destroy(other.gameObject);
        }
        else if(other.CompareTag("ColorsCollectibles"))
        {
            gameManagerInstance.EndedGameLoose();
            //vibrate mobiles phone: //Only activate with specifi if (mobile run) otherwise it pro
            //Handheld.Vibrate();

            Destroy(other.gameObject);
        }
        
    }
    #endregion
    #region PUBLIC_UNITY_METHODS
    #endregion


    #region PRIVATE_NON_MONOBEHAVIORS_METHODS
    #endregion
    #region PUBLIC_NON_MONOBEHAVIORS_METHODS
    #endregion

    #region TESTS METHODS_OR_SCRIPTS
    #endregion
}
