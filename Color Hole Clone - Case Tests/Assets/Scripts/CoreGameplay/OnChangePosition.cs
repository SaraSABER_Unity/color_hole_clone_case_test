﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
/// <summary>
/// On Change Position script
/// </summary>
public class PlayersController : MonoBehaviour
{
    #region PRIVATE_ATTRIBUTS
    Mesh generatedMesh;
    #endregion
    #region PUBLIC_ATTRIBUTS
    public PolygonCollider2D hole2DCollider;
    public PolygonCollider2D ground2Collider;
    public MeshCollider generatedMeshCollider;
    public Collider groundCollider;

    //Controll Sctipy
    public Transform target;
    public float flOffset;
    public Transform starterHolePos;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;
    
    //Incase we want scalling (gameplay test) for the game
    public float initialScale = 0.5f;
    #endregion


    #region PRIVATE_UNITY_METHODS
    // Start is called before the first frame update
    void Start()
    {
        //Turn off collisions for optimisi
        GameObject[] AllGOs = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        foreach(var go in AllGOs)
        {
            if(go.layer==LayerMask.NameToLayer("Obstacles"))
            {
                Physics.IgnoreCollision(go.GetComponent<Collider>(), generatedMeshCollider, true);
            }
        }
    }
    // Update is called once per physicals frame
    void FixedUpdate()
    {
        if(transform.hasChanged==true)
        {
            transform.hasChanged = false;
            hole2DCollider.transform.position = new Vector2(transform.position.x, transform.position.z);

            //In case we want to have scalling the hole we add the following code line bellow ( otherwise we suppress)
            hole2DCollider.transform.localScale = transform.localScale * initialScale;

            MakeHole2D();
            Make3DMeshCollider();
        }
    }
    //On Trigger Enters methods
    private void OnTriggerEnter(Collider other)
    {
        Physics.IgnoreCollision(other,groundCollider,true);
        Physics.IgnoreCollision(other, generatedMeshCollider, false);
    }
    private void OnTriggerExit(Collider other)
    {
        Physics.IgnoreCollision(other, groundCollider, false);
        Physics.IgnoreCollision(other, generatedMeshCollider, true);
    }
    #endregion
    #region PUBLIC_UNITY_METHODS
    #endregion


    #region PRIVATE_NON_MONOBEHAVIORS_METHODS
    //Make 2D Hole Method
    void MakeHole2D()
    {
        Vector2[] pointPositions = hole2DCollider.GetPath(0);
        for (int i=0;i<pointPositions.Length;i++)
        {
            //In case we dont want scalling the hole in GP
            //-->pointPositions[i] += (Vector2)hole2DCollider.transform.position;
            //In case we want scalling the hole inn gpp
            pointPositions[i]  = hole2DCollider.transform.TransformPoint(pointPositions[i]);
        }
        ground2Collider.pathCount = 2;
        ground2Collider.SetPath(1, pointPositions);   

    }
    //Make 3D Mesh Collider
    void Make3DMeshCollider()
    {
        if (generatedMesh != null)
            Destroy(generatedMesh);
        generatedMesh = ground2Collider.CreateMesh(true, true);
        generatedMeshCollider.sharedMesh = generatedMesh;
    }
    #endregion
    #region PUBLIC_NON_MONOBEHAVIORS_METHODS
    //Get Play position Methods
    public void StartData(BaseEventData myEvent)
    {
        if (((PointerEventData)myEvent).pointerCurrentRaycast.isValid)
        {
            
            offset =   starterHolePos.position- ((PointerEventData)myEvent).pointerCurrentRaycast.worldPosition;
            
            ///transform.position = ((PointerEventData)myEvent).pointerCurrentRaycast.worldPosition;
            //offset= Vector3.Distance(transform.position , ((PointerEventData)myEvent).pointerCurrentRaycast.worldPosition);
            ///offset = starterHolePos.position + ((PointerEventData)myEvent).pointerCurrentRaycast.worldPosition;
            ///Vector3 desiredPosition = ((PointerEventData)myEvent).pointerCurrentRaycast.worldPosition + offset;
            ///Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            ///transform.position = offset;
            ///starterHolePos.position = offset+transform.position;
            //transform.LookAt(target);
        }
    }

    //Move Play position Methods
    public void Move(BaseEventData myEvent) //I choosed to move according to the plane Collider, if this is an issues and you want to move by touch please tell me i will change to standard touch on mobile with clicks or touchCounts (I found this a new way to code away from standards for a test :_) ), Thanks you for letting me know your reply. 
    {
        if(((PointerEventData)myEvent).pointerCurrentRaycast.isValid)
        {

            
            
            //Controls 1
            Vector3 desiredPosition = ((PointerEventData)myEvent).pointerCurrentRaycast.worldPosition + offset;
            
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            
            transform.position = smoothedPosition;
            transform.position = new Vector3(smoothedPosition.x, starterHolePos.position.y, smoothedPosition.z);

            /*
            //Controls 2
            transform.position = ((PointerEventData)myEvent).pointerCurrentRaycast.worldPosition;
            */

            /*
            ///
            //Controls 3
            offset = starterHolePos.position - ((PointerEventData)myEvent).pointerCurrentRaycast.worldPosition;
            //or
            offset = transform.position - ((PointerEventData)myEvent).pointerCurrentRaycast.worldPosition;
            Vector3 desiredPosition = ((PointerEventData)myEvent).pointerCurrentRaycast.worldPosition + offset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);

            transform.position = smoothedPosition;
            starterHolePos.position = transform.position;
            */
        }
    }
    #endregion

    #region TESTS METHODS_OR_SCRIPTS
    #endregion
}